﻿namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.HttpServices
{
    public interface IHttpServiceFactory
    {
        public IHttpService CreateHttpService();

        public IHttpService CreateHttpService(string name);
    }
}