﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Enums;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Models
{
    public class Filter
    {
        public string Name { get; set; }
        public Operator Operator { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }
    }
}