﻿namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Models
{
    public class ListParameter
    {
        public Filter[] Filter { get; set; }
        public SortField SortField { get; set; }
        public Pagination Pagination { get; set; }
    }
}