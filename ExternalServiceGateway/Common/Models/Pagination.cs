﻿namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Models
{
    public class Pagination
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}