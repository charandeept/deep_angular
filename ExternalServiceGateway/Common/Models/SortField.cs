﻿namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Common.Models
{
    public class SortField
    {
        public string PropertyName { get; set; }
        public bool IsAscending { get; set; }
    }
}