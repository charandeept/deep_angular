﻿using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IRepository;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.IService;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Repository;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Repository.Context;
using Com.ACSCorp.AccountPlanning.ExternalServiceGateway.Service;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Text;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.DependencyResolver
{
    public static class DependenciesResolver
    {
        public static void ResolveDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SampleContext>(options => options.UseSqlServer(configuration.GetConnectionString("ConnectionString")));
            services.InjectServices();
            services.InjectRepositories();
        }

        internal static void InjectServices(this IServiceCollection services)
        {
            services.AddScoped<ISampleService, SampleService>();
        }

        internal static void InjectRepositories(this IServiceCollection services)
        {
            //services.AddScoped<ISampleRepository, SampleRepository>();
        }
    }
}