﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace Com.ACSCorp.AccountPlanning.ExternalServiceGateway.API.Controllers
{
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
    }
}