﻿using Microsoft.EntityFrameworkCore.Storage;

using System;
using System.Collections.Generic;
using System.Text;

namespace Com.ACSCorp.AccountPlanning.Service.IRepository
{
    public interface IBaseRepository
    {
        public IDbContextTransaction BeginTransaction();
    }
}