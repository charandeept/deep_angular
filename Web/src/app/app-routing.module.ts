import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './@core';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { SsoCallbackComponent } from './login/sso-callback/sso-callback.component';
import { ErrorComponent } from './shared';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'sso-callback',
    component: SsoCallbackComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: LayoutComponent,
    //canActivate: [AuthGuardService],
    children: [{
      path: 'dashboard',
      loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    }]
  },  
  {
    path: '',
    component: LayoutComponent,
    children: [{
      path: 'customer-info',
      loadChildren: () => import('./customer-info/customer-info.module').then(m => m.CustomerInfoModule),
    }]
  },
  { path: 'error', component: ErrorComponent },
  //This should be at the bottom only
  { path: '**', redirectTo: '/error' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
