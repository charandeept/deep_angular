import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule, HttpHelper } from './@core';
import { AppConfigService, AuthService, GlobalErrorService, GlobalEventManagerService, LoaderService, LoggerService, RouteService, StoreService } from './@core/services';
import { LayoutComponent } from './layout/layout.component';
import { LoginModule } from './login/login.module';
import { SharedComponentsModule } from './shared';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { DataForOverviewService } from './shared/Services/data-for-overview.service';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';

const coreServices = [
  AppConfigService,
  AuthService,
  GlobalErrorService,
  GlobalEventManagerService,
  LoaderService,
  LoggerService,
  RouteService,
  StoreService,
  HttpHelper
];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CoreModule,
    LoginModule,
    LayoutModule,
    SharedComponentsModule    
  ],
  providers: [coreServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
