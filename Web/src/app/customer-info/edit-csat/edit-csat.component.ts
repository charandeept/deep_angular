import { Component } from '@angular/core';
import { CsatRatingService } from './csat-rating.service';

@Component({
  selector: 'app-edit-csat',
  templateUrl: './edit-csat.component.html',
  styleUrls: ['./edit-csat.component.scss']
})
export class EditCSATComponent  {
  stars=[1,2,3,4,5];
  selectedRating:number=0;

  /**
   * @param csatServiceRating ="Instance of CSAT Service"
   */
  constructor(private csatServiceRating:CsatRatingService) { }

  /**
   *  Get the selected rating by the user and storing the selected data in the service
   * @param ratingNumber :  Used to get the Rating selected by the User in the front end 
   */
  updateRating(ratingNumber:number){
    this.selectedRating=ratingNumber;
    this.csatServiceRating.userSelectedRating=this.selectedRating
    console.log(this.csatServiceRating,'csat')
  }
 
}
