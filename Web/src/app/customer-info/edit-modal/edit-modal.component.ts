import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class EditModalComponent  {
  bussinessContent: boolean = true;
  csatContent: boolean = false;
  vendorsContent: boolean = false;
  customerMoodContent:boolean=false;

  
  
  constructor() {
  }

  /**
   * Is used to select the tab in the modal
   * @param activeTab ="Acticvate Tab"
   */
  tab(activeTab: any) {
    this.bussinessContent = false;
    this.csatContent = false;
    this.vendorsContent = false;
    this.customerMoodContent=false;

    if (activeTab == 'bussiness') {
      this.bussinessContent = true;
    }
    else if (activeTab == 'CSAT') {
      this.csatContent = true;
    }
    else if (activeTab == 'vendors') {
      this.vendorsContent = true;
    }
    else if(activeTab=='customerMood'){
      this.customerMoodContent=true;
    }
  }
}
