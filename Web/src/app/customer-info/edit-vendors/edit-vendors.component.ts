import { Inject } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomerDetailsService } from '../services/customer-details.service';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-edit-vendors',
  templateUrl: './edit-vendors.component.html',
  styleUrls: ['./edit-vendors.component.scss']
})


export class EditVendorsComponent implements OnInit {
  serviceType: any = [];
  userDetails: any = [];
  displayedColumns: string[] = ['vendor', 'service'];
  dataSource: any;

  @ViewChild(MatSort) sort!: MatSort;


  /**
   * 
   * @param customerDetails 
   * @param userData  fetched data from the Overview Card
   */
  constructor(customerDetails: CustomerDetailsService,
    @Inject(MAT_DIALOG_DATA) public userData: any,
    private _liveAnnouncer: LiveAnnouncer) {
    this.userDetails = userData.vendors
    this.serviceType = customerDetails.timeZoneOption
    this.fetchingData()
  }

  vendorForm = new FormGroup({
    vendorName: new FormControl(''),
    vendorServiceType: new FormControl(''),
    vendorAddNew: new FormControl('')
  })

  ngOnInit(): void {
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.userDetails);
    }, 100);

  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  fetchingData() {
    this.vendorForm.patchValue({
      vendorName: this.userData.Website,
      vendorServiceType: this.userData.Timezone
    })
  }

  /** Announce the change in sort state for assistive technology. */
  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

}