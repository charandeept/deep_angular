import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataForOverviewService } from 'src/app/shared/Services/data-for-overview.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerDetailsService {
  data: any = [];
  public industryOption: any = [];
  deliveryModelOption: any = [];
  deliveryManagerOption: any = [];
  headquatersOption: any = [];
  timeZoneOption: any = [];
  vendors: any = [];
  constructor(public customerDetails: DataForOverviewService) 
  {
    this.customerDetails.getConfig().subscribe((data: any) => {
      this.data = data;
      this.data.forEach((element: any) => {
        this.industryOption.push(element.Industry)
        this.deliveryModelOption.push(element.DeliveryModel)
        this.headquatersOption.push(element.Headquarters)
        this.timeZoneOption.push(element.Timezone)
        this.deliveryManagerOption.push(element.DeliveryManager)
        this.vendors.push({ [element.id]: element.vendors })
      });
    })
  }
//   observer = new Observable(x =>
//     x.next(this.industryOption)) 
// }
}
