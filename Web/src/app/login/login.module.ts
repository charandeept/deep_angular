import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SsoCallbackComponent } from './sso-callback/sso-callback.component';

@NgModule({
  declarations: [LoginComponent, SsoCallbackComponent],
  imports: [
    CommonModule,
    LoginRoutingModule
  ],
  exports: [LoginComponent, SsoCallbackComponent]
})
export class LoginModule { }
