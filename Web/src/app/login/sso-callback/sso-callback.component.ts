import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AuthService, CoreConstant, StoreService } from 'src/app/@core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-sso-callback',
  templateUrl: './sso-callback.component.html',
  styleUrls: ['./sso-callback.component.scss']
})
export class SsoCallbackComponent implements OnInit {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private loginService: LoginService,
    private authService: AuthService,
    private storageService: StoreService) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((paramMap: ParamMap) => {
      let token = paramMap.get('access_token');
      if (token) {
        //set token in storage
        this.storageService.setDataWithoutJSON(CoreConstant.storageKey.authToken, token);

        //TODO
        // this.loginService.get().subscribe((res: any) => {
        //   console.log(res);
        //   this.authService.onLoginHandler(res, token as string);
        //   this.router.navigate(['dashboard']);
        // });
        this.router.navigate(['dashboard']);
      }
      else {
        this.router.navigate(['login']);
      }
    });
  }

}
