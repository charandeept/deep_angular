//#region angular imports

import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, Input, SimpleChanges, ViewChild, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItemModel } from './models/menu-item.model';
import { NavMenuModel } from './models/nav-menu.model';

//#endregion angular imports

//#region functional/model imports

import { NavBarService } from './nav-bar.service';

//#endregion functional/model imports

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.html',
  styleUrls: ['./nav-bar.scss']
})
export class NavBarComponent implements OnInit, OnDestroy, OnChanges {

  //#region input properties

  @Input() show: boolean = false;
  
  @ViewChild('sideMenu')


  navBar!: { toggle: () => void; };
  navbarMenu: MenuItemModel[] = [];
  index!: number;
  mobileQuery: MediaQueryList;
  private mobileQueryListener: () => void;
  //#endregion input properties


  //#region constructor

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private service: NavBarService, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 992px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);
  }

  //#endregion constructor

  //#region life cycle hooks

  ngOnInit() {
    this.service.getNavMenu().subscribe((menu: NavMenuModel) => {
      this.navbarMenu.push(...menu.menuList);
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['show']) {
      this.navBar?.toggle();
    }
  }

  ngOnDestroy() {
    this.mobileQuery.removeEventListener('change', this.mobileQueryListener)
  }

  //#endregion cycle hooks

}
