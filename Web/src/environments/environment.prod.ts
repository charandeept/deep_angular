export const environment = {
  production: true,
  siteBaseUrl: "",
  apiBaseUrl: "<apiURL>",
  ssoUrl: "https://acs-sso-accelerator.azurewebsites.net/account/login/1009?returnUrl=http://172.16.0.87:50501/sso-callback"
};